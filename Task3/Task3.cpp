#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
�������� 3.
������� ������� Calc (A, B, Op) ������� ����, �� ������ ���
����������� ������� ������� A �� B ���� � ������������ �������� �
������� ���������. ��� �������� ����������� ����� ���������� Op: 1 �
���������, 2 � ��������, 3 � �������, ���� �������� � ���������. ��
��������� Calc �������� ��� ����� A � B ��������, �� ������������
������ ������ N1, N2, N3.

*/
float Calc(float a,float b,int op);

int main() {
	printf("Enter a, b, op:\n");
	float a, b;
	int op;
	scanf("%f%f%d", &a, &b, &op);
	printf("Result is %f", Calc(a, b, op));
	return 0;
}
float Calc(float a, float b, int op) {
	switch (op)
	{
		case 1:
			return a - b;
		case 2:
			return a * b;
		case 3:
			return a / b;
		default:
			return a + b;
	}
}