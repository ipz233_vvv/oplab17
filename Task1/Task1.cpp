#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
�������� 1.
�������� �������, ��� �������� ����� �� �������. �������� ������
��� ������� �: �������� ������, ��������� ������ (�����) � �����
������ (������� ����).
*/
float income(float start, float percent, int days) {
	float daily_percent = percent / 365;
	start *= daily_percent * days /100;
	return start;
}
int main() {
	float deposit;
	float rate;
	int term;
	printf("Enter the amount of the deposit, the interest rate (annual) and the term contribution:\n");
	scanf("%f%f%d", &deposit, &rate, &term);
	printf("You will get %f more money units after %d days", income(deposit,rate,term));
	return 0;
}